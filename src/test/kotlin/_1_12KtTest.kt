import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{
    @Test
    fun randomCelcius(){
        assertEquals(91.4,celciusToFahrenheit(33.0))
    }
    @Test
    fun negativeCelcius(){
        assertEquals(-18.4,celciusToFahrenheit(-28.0))
    }
    @Test
    fun biggerCelcius(){
        assertEquals(9423666.860000001,celciusToFahrenheit(5235352.7))
    }
    @Test
    fun ceroCelcius(){
        assertEquals(32.0,celciusToFahrenheit(0.0))
    }
}