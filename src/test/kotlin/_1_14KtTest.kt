import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{

    @Test
    fun randomClientsPrice(){
        assertEquals(20.0, pricePerClient(15,300.0))
    }
    @Test
    fun negativeClients(){
        assertEquals(-35.0, pricePerClient(-12,420.0))
    }
    @Test
    fun negativePrice(){
        assertEquals(-26.666666666666668, pricePerClient(9,-240.0))
    }
    @Test
    fun biggerPrice(){
        assertEquals(5910.714285714285, pricePerClient(21,124125.0))
    }
    @Test
    fun ceroClientsPrice(){
        assertEquals(Double.NaN, pricePerClient(0,0.0))
    }
}