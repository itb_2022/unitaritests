import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{
    @Test
    fun randomDouble(){
        assertEquals(42.0, toDouble(42))
    }
    @Test
    fun negativeDouble(){
        assertEquals(-42.0, toDouble(-42))
    }
    @Test
    fun biggerDouble(){
        assertEquals(1245152656.0, toDouble(1245152656))
    }
    @Test
    fun ceroDouble(){
        assertEquals(0.0, toDouble(0))
    }
}