import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_15KtTest{
    @Test
    fun randomSeconds(){
        assertEquals(33,add1Second(32))
    }
    @Test
    fun limitSeconds(){
        assertEquals(0,add1Second(59))
    }
    @Test
    fun oneMinute(){
        assertEquals(1,add1Second(60))
    }
    @Test
    fun ceroSeconds(){
        assertEquals(1,add1Second(0))
    }
}