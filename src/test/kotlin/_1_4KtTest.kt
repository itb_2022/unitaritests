import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{
    @Test
    fun randomWidthWide(){
        assertEquals(12,area(4,3))
    }
    @Test
    fun negativeWidthWide(){
        assertEquals(100,area(-10,-10))
    }
    @Test
    fun positiveWidthnegativeWide(){
        assertEquals(100,area(10,-10))
    }
    @Test
    fun negativeWidthPositiveWide(){
        assertEquals(100,area(-10,10))
    }
}