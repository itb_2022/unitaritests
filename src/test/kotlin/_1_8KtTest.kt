import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{
    @Test
    fun multiplyRandomNumber(){
        assertEquals(42, numMultipliedPerTwo(21))
    }
    @Test
    fun multiplyNegativeNumber(){
        assertEquals(-90, numMultipliedPerTwo(-45))
    }
    @Test
    fun multiplyBiggerNumber(){
        assertEquals(48704730, numMultipliedPerTwo(24352365))
    }
    @Test
    fun multiplyPer0(){
        assertEquals(0, numMultipliedPerTwo(0))
    }
}