import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{
    @Test
    fun sumNumbers(){
        assertEquals(50, sumTwoNumbers(25,25))
    }
    @Test
    fun sumNegativeNumbers(){
        assertEquals(-50, sumTwoNumbers(50,-100))
    }
    @Test
    fun sumBigNumbers(){
        assertEquals(1000000000, sumTwoNumbers(500000000,500000000))
    }
}