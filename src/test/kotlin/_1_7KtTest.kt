import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{
    @Test
    fun sum1ToRandomNumber(){
        assertEquals(32,plus1(31))
    }
    @Test
    fun sum1ToNegativeNumber(){
        assertEquals(-30,plus1(-31))
    }
    @Test
    fun sum1ToBiggerNumber(){
        assertEquals(324235624,plus1(324235623))
    }
}