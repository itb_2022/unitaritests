import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{
    @Test
    fun randomDiameter (){
        assertEquals(113.09733552923255, pizzaSize(12.0))
    }
    @Test
    fun negativeDiameter (){
        assertEquals(415.4756284372501, pizzaSize(-23.0))
    }
    @Test
    fun biggerNumber (){
        assertEquals(148616.96746888215,pizzaSize(435.0))
    }
    @Test
    fun piSize (){
        assertEquals(Math.PI,pizzaSize(2.0))
    }
    @Test
    fun ceroDiamater (){
        assertEquals(0.0,pizzaSize(0.0))
    }
}