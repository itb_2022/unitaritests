import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{
    @Test
    fun randomNums(){
        assertEquals(9,operation(5,4,3,2))
    }
    @Test
    fun negativeNums(){
        assertEquals(0,operation(-4,3,-2,1))
    }
    @Test
    fun allNegativeNums(){
        assertEquals(0, operation(-4,-3,-2,-1))
    }
    @Test
    fun biggerNums(){
        assertEquals(50000, operation(300,200,100,200))
    }
}