import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{
    @Test
    fun randomTemperature (){
        assertEquals(36.0,sumTemps(32.0,4.0))
    }
    @Test
    fun negativeNewTemperature (){
        assertEquals(28.0,sumTemps(32.0,-4.0))
    }
    @Test
    fun negativeTemperature (){
        assertEquals(-28.0,sumTemps(-32.0,4.0))
    }
    @Test
    fun biggerTemperature (){
        assertEquals(1.252480471564E9,sumTemps(124125.0,1252356346.564))
    }
    @Test
    fun ceroTemperature (){
        assertEquals(0.0,sumTemps(0.0,0.0))
    }
    @Test
    fun ceroTemperatureRandomNewTemperature (){
        assertEquals(12.0,sumTemps(0.0,12.0))
    }
}