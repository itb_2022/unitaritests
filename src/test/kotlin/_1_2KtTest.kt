import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{
    @Test
    fun doubleNumber(){
        assertEquals(4, doubleNum(2))
    }
    @Test
    fun negativeNumber(){
        assertEquals(-4, doubleNum(-2))
    }
    @Test
    fun ceroNumber(){
        assertEquals(0, doubleNum(0))
    }
}