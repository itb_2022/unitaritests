import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{
    @Test
    fun randomWidthWideHigh(){
        assertEquals(756, volum(12,7,9))
    }
    @Test
    fun randomNegativePositiveWidthPositiveWideHigh(){
        assertEquals(-1000, volum(-10,10,10))
    }
    @Test
    fun allWidthWideHighNegative(){
        assertEquals(-58788, volum(-213,-23,-12))
    }
    @Test
    fun ceroWidthWideHigh(){
        assertEquals(0, volum(0,0,0))
    }
}