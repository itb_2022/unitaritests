import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{
    @Test
    fun randomPupitres(){
        assertEquals(38, pupiter(23,34,18))
    }
    @Test
    fun negativePupitres(){
        assertEquals(21, pupiter(-21,32,31))
    }
    @Test
    fun biggerPupitres(){
        assertEquals(14326, pupiter(3204,23124,2324))
    }
}