import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{
    @Test
    fun randomNumbers (){
        assertEquals(46.51162790697675, discountPercent(129.0,69.0))
    }
    @Test
    fun samePriceBoth (){
        assertEquals(0.0, discountPercent(100.0,100.0))
    }
    @Test
    fun halfPriceDiscounted (){
        assertEquals(50.0, discountPercent(60.0,30.0))
    }
    @Test
    fun ceroDiscounted (){
        assertEquals(0.0, discountPercent(60.0,60.0))
    }
}