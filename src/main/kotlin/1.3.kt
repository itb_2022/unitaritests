/*
* AUTHOR: David Gómez
*/

import java.util.*

fun sumTwoNumbers(num1:Int,num2:Int):Int{
    return num1+num2
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix dos números: ")
    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()
    println("Aquesta es la suma: ${println(sumTwoNumbers(num1,num2))}")
}