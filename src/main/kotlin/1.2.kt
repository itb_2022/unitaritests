/*
* AUTHOR: David Gómez
*/

import java.util.*

fun doubleNum(num: Int): Int{
    return num*2
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número: ")
    val num = scanner.nextInt()
    println("Aquest es el doble: ${doubleNum(num)}")
}