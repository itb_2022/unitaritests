/*
* AUTHOR: David Gómez
*/

import java.util.*

fun sumTemps (temp: Double, tempNew:Double):Double {
    return temp+tempNew
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la temperatura:")
    val temp = scanner.nextDouble()
    println("Ara introdueix el seu increment:")
    val tempNew = scanner.nextDouble()
    print("La temperatura actual es de ${sumTemps(temp,tempNew)}")
}