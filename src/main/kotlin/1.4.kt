/*
* AUTHOR: David Gómez
*/

import java.util.*
import kotlin.math.abs

fun area(width: Int, wide: Int): Int{
    return abs(width*wide)
}

fun main() {
        val scanner = Scanner(System.`in`)
        println("Introdueix la llargada: ")
        val width = scanner.nextInt()
        println("Introdueix la amplada: ")
        val wide = scanner.nextInt()
        println("Aquesta és la seva àrea: ${area(width,wide)}")
}