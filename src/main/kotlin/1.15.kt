/*
* AUTHOR: David Gómez
*/

import java.util.*

fun add1Second (seconds:Int):Int {
    return (seconds+1)%60
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número de segons (menor a 60s):")
    val second = scanner.nextInt()
    println("El número introduït, més un segon, es de ${add1Second(second)}s")
}