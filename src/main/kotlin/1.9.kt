/*
* AUTHOR: David Gómez
*/

import java.util.*

fun discountPercent(price:Double,discountedPrice:Double):Double{
    return (100-100*(discountedPrice/price))
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el preu d'un producte:")
    val price = scanner.nextDouble()
    println("Ara introdueix el preu després d'aplicar-li el descompte:")
    val discountedPrice = scanner.nextDouble()
    println("Aquest és el percentatge del descompte: ${(discountPercent(price,discountedPrice))}")
}