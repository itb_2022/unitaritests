/*
* AUTHOR: David Gómez
*/

import java.util.*

fun pricePerClient(clients:Int,price:Double):Double {
    return price/clients
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els començals:")
    val clients = scanner.nextInt()
    println("Introdueix el preu que han de pagar:")
    val price = scanner.nextDouble()
    println("El preu per començal es de: ${pricePerClient(clients,price)}")
}