/*
* AUTHOR: David Gómez
*/

import java.util.*

fun pupiter(classe1:Int,classe2:Int,classe3:Int):Int{
    return (classe1/2 + classe2/2 + classe3/2 + classe1%2 + classe2%2 + classe3%2)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el número d'alumnes de la classe 1:")
    val classe1 = scanner.nextInt()
    println("Introdueix el número d'alumnes de la classe 2:")
    val classe2 = scanner.nextInt()
    println("Introdueix el número d'alumnes de la classe 3:")
    val classe3 = scanner.nextInt()
    print("Es necessiten ${pupiter(classe1,classe2,classe3)} pupitres")
}