/*
* AUTHOR: David Gómez
*/

import java.util.*

fun toDouble (num: Int):Double{
    return num.toDouble()
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un enter:")
    val num = scanner.nextInt()
    println("El seu 'double' es: ${toDouble(num)}")
}