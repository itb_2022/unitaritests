/*
* AUTHOR: David Gómez
*/

import java.util.*

fun operation(num1:Int,num2:Int,num3:Int,num4:Int):Int{
    return (num1 + num2) * (num3 % num4)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("A continuació introdueix els valors per realitzar l'operació")
    println("Introdueix el número 1:")
    val num1 = scanner.nextInt()
    println("Introdueix el número 2:")
    val num2 = scanner.nextInt()
    println("Introdueix el número 3:")
    val num3 = scanner.nextInt()
    println("Introdueix el número 4:")
    val num4 = scanner.nextInt()
    println("Aquest es el resultat de l'operació: ${operation(num1,num2,num3,num4)}")
}