/*
* AUTHOR: David Gómez
*/

import java.util.*

fun pizzaSize (diameter:Double):Double{
    val radi = diameter/2
    return (radi*radi)*Math.PI
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el diàmetre de la pizza:")
    val diameter = scanner.nextDouble()
    print("La seva superfície es de: ${pizzaSize(diameter)}")
}