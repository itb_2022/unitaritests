/*
* AUTHOR: David Gómez
*/

import java.util.*

fun numMultipliedPerTwo(num:Int):Int{
    return num*2
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()
    println("Aquest es el doble del valor introduït: ${numMultipliedPerTwo(num)}")
}