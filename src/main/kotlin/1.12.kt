/*
* AUTHOR: David Gómez
*/

import java.util.*

fun celciusToFahrenheit(celcius:Double):Double{
    return ((celcius*9.0/5.0)+32.0)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els graus en celcius:")
    val celcius = scanner.nextDouble()
    println("$celcius celcius, el seu valor en farenheits es de: ${celciusToFahrenheit(celcius)}")
}