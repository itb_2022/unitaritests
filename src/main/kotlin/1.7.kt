/*
* AUTHOR: David Gómez
*/

import java.util.*

fun plus1(num:Int):Int{
    return num+1
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.nextInt()
    print("Després del $num , el següent es el ${plus1(num)} ")
}