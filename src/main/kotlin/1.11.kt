/*
* AUTHOR: David Gómez
*/

import java.util.*

fun volum(width:Int,wide:Int,high:Int):Int{
    return width*wide*high
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la llargada de l'habitació:")
    val width = scanner.nextInt()
    println("Introdueix l'amplada de l'habitació:")
    val wide = scanner.nextInt()
    println("Introdueix l'altura de l'habitació:")
    val high = scanner.nextInt()
    println("Aquest es el seu volum d'aire: ${volum(width,wide,high)}")
}